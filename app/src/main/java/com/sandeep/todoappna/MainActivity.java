package com.sandeep.todoappna;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sandeep.todoappna.db.TodoListDBAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements ItemClickListener {
    private AppCompatEditText etTodo, etTodoPlace;
    private AppCompatTextView tvNoTodos;
    private RecyclerView rvTodos;
    private TodoAdapter adapter;
    private ArrayList<Todo> todos;
    private TodoListDBAdapter todoListDBAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etTodo = findViewById(R.id.et_todo);
        etTodoPlace = findViewById(R.id.et_todo_place);

        tvNoTodos = findViewById(R.id.tv_no_todos);

        AppCompatButton btnAdd = findViewById(R.id.btn_add);

        rvTodos = findViewById(R.id.rv_todos);
        rvTodos.setLayoutManager(new LinearLayoutManager(this));
        todos = new ArrayList<>();

        todoListDBAdapter = TodoListDBAdapter.getInstance(this);

        adapter = new TodoAdapter(this, todos);
        rvTodos.setAdapter(adapter);

        refreshTodos();

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Todo todo = new Todo();
                todo.setTodo(etTodo.getText().toString());
                todo.setPlace(etTodoPlace.getText().toString());
                if (addTodo(todo)) {
                    etTodo.setText("");
                    etTodoPlace.setText("");
                    refreshTodos();
                } else {
                    Toast.makeText(MainActivity.this, "Something went wrong!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshTodos();
    }

    private void refreshTodos() {
        todos.clear();
        todos.addAll(todoListDBAdapter.getAllTodos());

        if (todos.size() > 0) {
            rvTodos.setVisibility(View.VISIBLE);
            tvNoTodos.setVisibility(View.GONE);
        } else {
            rvTodos.setVisibility(View.GONE);
            tvNoTodos.setVisibility(View.VISIBLE);
        }

        adapter.notifyDataSetChanged();
    }

    private boolean addTodo(@NotNull Todo todo) {
        return todoListDBAdapter.insert(todo.getTodo(), todo.getPlace());
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(AppConstants.TODO_DATA, todos.get(position));
        intent.putExtra(AppConstants.TODO_POSITION, position);
        startActivity(intent);
    }
}
