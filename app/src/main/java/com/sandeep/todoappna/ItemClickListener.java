package com.sandeep.todoappna;

public interface ItemClickListener {
    void onItemClick(int position);
}
