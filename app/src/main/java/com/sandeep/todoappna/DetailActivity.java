package com.sandeep.todoappna;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.sandeep.todoappna.db.TodoListDBAdapter;

import java.util.Locale;

public class DetailActivity extends AppCompatActivity {
    private AppCompatEditText etTodoNameModify, etTodoPlaceModify;
    private AppCompatTextView tvTodoId, tvTodoName, tvTodoPlace, tvTodoRemoved;
    private LinearLayout llTodo;
    private TodoListDBAdapter todoListDBAdapter;
    private Todo todo;
    private int selectedTodoPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        etTodoNameModify = findViewById(R.id.et_todo_name_modify);
        etTodoPlaceModify = findViewById(R.id.et_todo_place_modify);

        llTodo = findViewById(R.id.ll_todo);

        tvTodoId = findViewById(R.id.tv_todo_ids);
        tvTodoName = findViewById(R.id.tv_todo_name);
        tvTodoPlace = findViewById(R.id.tv_todo_place);
        tvTodoRemoved = findViewById(R.id.tv_todo_removed);

        AppCompatButton btnRemove = findViewById(R.id.btn_remove);
        AppCompatButton btnModify = findViewById(R.id.btn_modify);

        todoListDBAdapter = TodoListDBAdapter.getInstance(this);

        todo = (Todo) getIntent().getSerializableExtra(AppConstants.TODO_DATA);
        selectedTodoPosition = getIntent().getIntExtra(AppConstants.TODO_POSITION, 0);

        if (todo != null) {
            tvTodoId.setText(String.format(Locale.ENGLISH, "%d", todo.getId()));
            tvTodoName.setText(todo.getTodo());
            tvTodoPlace.setText(todo.getPlace());
            tvTodoRemoved.setVisibility(View.GONE);
            llTodo.setVisibility(View.VISIBLE);
        } else {
            tvTodoRemoved.setVisibility(View.VISIBLE);
            llTodo.setVisibility(View.GONE);
        }

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (removeTodo(todo.getId())) {
                    llTodo.setVisibility(View.GONE);
                    tvTodoRemoved.setVisibility(View.VISIBLE);
                    onBackPressed();
                } else {
                    Toast.makeText(DetailActivity.this, "No such Task ID exist!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnModify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etTodoNameModify.getText().toString();
                String place = etTodoPlaceModify.getText().toString();
                if (TextUtils.isEmpty(name)) {
                    Toast.makeText(DetailActivity.this, "Please enter name to modify!!", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(place)) {
                    Toast.makeText(DetailActivity.this, "Please enter place to modify!!", Toast.LENGTH_SHORT).show();
                } else {
                    if (modifyTodo(todo.getId(), name, place)) {
                        etTodoNameModify.setText("");
                        etTodoPlaceModify.setText("");
                        refreshTodo();
                    } else {
                        Toast.makeText(DetailActivity.this, "Cannot modify!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void refreshTodo() {
        todo = todoListDBAdapter.getAllTodos().get(selectedTodoPosition);
        tvTodoId.setText(String.format(Locale.ENGLISH, "%d", todo.getId()));
        tvTodoName.setText(todo.getTodo());
        tvTodoPlace.setText(todo.getPlace());
    }

    private boolean removeTodo(int id) {
        return todoListDBAdapter.delete(id);
    }

    private boolean modifyTodo(int id, String todoName, String todoPlace) {
        return todoListDBAdapter.update(id, todoName, todoPlace);
    }
}
